/**
 * Created by andrew on 8/4/17.
 */

var cmdKey = "ctrl";
var slash = "\\";

var quill;
var file;
var fs = require("fs");
var win = nw.Window.get();

var Delta = Quill.import('delta');
var change;
var toolboxWin;

$(document).ready(function () {
    initiate();
});
function initiate(){
    quill = createQuill();
    autoSave();
    var val = sessionStorage.getItem('editor');
    if(val == "null"){
        file=null;
    }
    if(val != null){
        loadFile(val);
    }
}

function autoSave(){
    change = new Delta();
    quill.on('text-change', function(delta) {
        change = change.compose(delta);
        $("#file-name").css("color","red");
    });
    setInterval(function() {
        if (change.length() > 0) {
            console.log('Saving changes', change);
            if(file){
                saveFile();
                change = new Delta();
            }
        }
    }, 5*1000);
    win.on('close', function() {
        if (change.length() > 0) {
            if (confirm("Are you sure you want to leave? You have unsaved changes.") == true) {
                if (toolboxWin != null)
                    toolboxWin.close(true);
                this.close(true);
            }
        }
        else{
            if (toolboxWin != null)
                toolboxWin.close(true);
            this.close(true);
        }
    });
}

function loadText(text){
    var obj = JSON.parse(text);
    $("#file-name").text(file.substring(file.lastIndexOf(slash)+1));
    setTimeout(function(){
        quill.setContents(obj);
    }, 1);
}
function loadFile(filename){
    fs.readFile(filename, 'utf8', function (err,data) {
        if (err) {
            console.log(err);
        }
        console.log("READ");
        file = filename;

        loadText(data);
    });
}
function chooseFile(name, callback) {
    var chooser = $(name);
    chooser.change(function(evt) {
        callback($(this).val());
    });
    chooser.trigger('click');
}

function newFile(){
    sessionStorage.setItem('editor', null);
    window.open("editor.html");
}
function openFile(){
    chooseFile("#openFileDialog", function(filename){
        if(file != null){
            sessionStorage.setItem('editor', filename);
            window.open("editor.html");
        }
        else{
            loadFile(filename)
        }
    });
}
function saveFile(){
    if(file!=null){
        fs.writeFile(file, JSON.stringify(quill.getContents()), function(err) {
            if(err) {
                console.log(err);
            } else {
                console.log("The file was saved!");
                $("#file-name").css("color","green");
            }
        });
    }
    else{
        saveasFile();
    }
}
function saveasFile(){
    chooseFile("#saveFileDialog", function(filename){
        fs.writeFile(filename, JSON.stringify(quill.getContents()), function(err) {
            file = filename;
            $("#file-name").text(file.substring(file.lastIndexOf(slash)+1));
            if(err) {
                console.log(err);
            } else {
                console.log("The file was saved!");
                $("#file-name").css("color","green");
            }
        });
    });
}
function printFile(){
    var filename = file;
    if(file=null){
        filename="";
    }
    var a = filename.lastIndexOf(".");
    if(a>0){
        filename.substring(0,a);
    }
    $("#file-name").hide();
    $(".ql-toolbar").hide();
    win.print({
        autoprint:false,
        pdf_path:filename+".pdf",
        landscape:false,
        headerFooterEnabled: false,
        shouldPrintBackgrounds: true
    });
    $("#file-name").show();
    $(".ql-toolbar").show();
}

function openToolbox(com){
    if(file){
        sessionStorage.setItem("tbcommand",com);
        sessionStorage.setItem("tbfile",file);
        toolboxWin = window.open("toolbox.html");
        toolboxWin.on('closed', function() {
            toolboxWin = null;
        });
    }
    else{
        alert("Please save before opening the toolbox.");
    }
}