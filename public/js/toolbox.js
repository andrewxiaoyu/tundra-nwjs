/**
 * Created by Andrew Wang on 8/23/2017.
 */

var win = nw.Window.get();
var file;

$(document).ready(function(){
    setupButtons();
    setupToolbox();
    setupMath();
});
function setupButtons(){
    $(".type-panel").hover(function(){
        $(this).find(".type-icon").addClass("type-icon-hover");
        $(this).find(".type-text").addClass("type-text-hover");
    }, function(){
        $(this).find(".type-icon").removeClass("type-icon-hover");
        $(this).find(".type-text").removeClass("type-text-hover");
    });
    $(".type-panel").click(function(){
        var text = $(this).find(".type-text").text();
        $("#type-select").hide();
        $("#"+text.toLowerCase()).parent().show();
    });
}
function setupToolbox(){
    var cmd = sessionStorage.getItem("tbcommand");
    file = sessionStorage.getItem("tbfile");
    sessionStorage.setItem("tbcommand",null);
    sessionStorage.setItem("tbfile",null);

    $("#back").click(function () {
        $(".feature").hide();
        $("#type-select").show();
    });
    switch(cmd){
        case "record":
            break;
        case "formula":
            break;
        default:
            console.log("Hello!");
    }
    win.height = 300;
    win.width = 300;
    win.setResizable(false);
}

function record(){

}
function setupMath(){
    var mathFieldSpan = document.getElementById('math-input');
    var latexSpan = document.getElementById('math-latex');

    var MQ = MathQuill.getInterface(2);
    var mathField = MQ.MathField(mathFieldSpan, {
        spaceBehavesLikeTab: true,
        handlers: {
            edit: function() {
                latexSpan.textContent = mathField.latex();
            }
        }
    });
    new Clipboard('#math-insert');
}