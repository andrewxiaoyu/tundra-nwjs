/**
 * Created by Andrew Wang on 8/20/2017.
 */

var cmdKey = "ctrl";

$(document).ready(function () {
    initMenu();
});

function initMenu(){
    var win = nw.Window.get();
    var menubar = new nw.Menu({ type: 'menubar' });

    menubar.append(new nw.MenuItem({ label: 'File', submenu: fileMenu()}));
    menubar.append(new nw.MenuItem({ label: 'Theme', submenu: editMenu()}));
    menubar.append(new nw.MenuItem({ label: 'Tools', submenu: toolMenu()}));
    menubar.append(new nw.MenuItem({ label: 'Help', submenu: helpMenu()}));
    win.menu = menubar;
}
function fileMenu(){
    var fileMenu = new nw.Menu();
    fileMenu.append(new nw.MenuItem({
        label: 'New',
        click: function(){newFile()},
        key: "n",
        modifiers:cmdKey
    }));
    fileMenu.append(new nw.MenuItem({
        label: 'Open',
        click: function(){openFile()},
        key: "o",
        modifiers:cmdKey
    }));
    fileMenu.append(new nw.MenuItem({ type: 'separator' }));
    fileMenu.append(new nw.MenuItem({
        label: 'Save',
        click: function(){saveFile()},
        key: "s",
        modifiers:cmdKey
    }));
    fileMenu.append(new nw.MenuItem({
        label: 'Save As',
        click: function(){saveasFile()},
        key: "s",
        modifiers:cmdKey+"shift"
    }));
    fileMenu.append(new nw.MenuItem({ type: 'separator' }));
    fileMenu.append(new nw.MenuItem({
        label: 'Print',
        click: function(){printFile()},
        key: "p",
        modifiers:cmdKey
    }));
    return fileMenu;
}
function editMenu(){
    var editMenu = new nw.Menu();
    editMenu.append(new nw.MenuItem({
        label: 'Light',
        click: function(){
            $('#theme').attr('href','css/styles/tundra.css');
            $('#quillstyle').attr('href','css/other/quill.snow.css');
        }
    }));
    editMenu.append(new nw.MenuItem({
        label: 'Dark',
        click: function(){
            $('#theme').attr('href','css/styles/dark.css');
            $('#quillstyle').attr('href','css/other/quill.snow.dark.css');
        }
    }));
    return editMenu;
}
function toolMenu(){
    var toolMenu = new nw.Menu();
    toolMenu.append(new nw.MenuItem({
        label: 'Toolbox',
        key: "t",
        modifiers:cmdKey+"shift",
        click:function(){ openToolbox(null) }
    }));
    toolMenu.append(new nw.MenuItem({ type: 'separator' }));
    toolMenu.append(new nw.MenuItem({
        label: 'Recording',
        key: "r",
        modifiers:cmdKey+"shift",
        click:function(){ openToolbox("record") }
    }));
    toolMenu.append(new nw.MenuItem({
        label: 'Formula',
        key: "g",
        modifiers:cmdKey+"shift",
        click:function(){ openToolbox("formula") }
    }));
    return toolMenu;
}
function helpMenu(){
    var helpMenu = new nw.Menu();

    helpMenu.append(new nw.MenuItem({
        label: 'LaTeX',
        click: function(){window.open("http://reu.dimacs.rutgers.edu/Symbols.pdf")}
    }));
    helpMenu.append(new nw.MenuItem({ type: 'separator' }));
    helpMenu.append(new nw.MenuItem({
        label: 'Python',
        click: function(){window.open("http://www.cogsci.rpi.edu/~destem/igd/python_cheat_sheet.pdf")}
    }));
    helpMenu.append(new nw.MenuItem({
        label: 'Java',
        click: function(){window.open("https://www.ktbyte.com/resources/pdf/KTBYTEJavaCheatSheet.pdf")}
    }));
    helpMenu.append(new nw.MenuItem({
        label: 'C#',
        click: function(){window.open("http://www.digilife.be/quickreferences/qrc/core%20csharp%20and%20.net%20quick%20reference.pdf")}
    }));
    helpMenu.append(new nw.MenuItem({
        label: 'HTML',
        click: function(){window.open("https://web.stanford.edu/group/csp/cs21/htmlcheatsheet.pdf")}
    }));
    return helpMenu;
}