/**
 * Created by Andrew Wang on 6/20/2017.
 */

// var quill = require("quill");
// var options={
//     modules: {
//         syntax:true,
//         formula: true,
//         toolbar:[
//             [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
//             [{ 'align': [] }],
//             [{ 'color': [] }, { 'background': [] }],
//             ['bold', 'italic', 'underline'],
//             ['strike','code'],
//             [{ 'list': 'ordered'}, { 'list': 'bullet' }],
//             [{ 'indent': '-1'}, { 'indent': '+1' }],
//             [{ 'script': 'sub'}, { 'script': 'super' }],
//             ['blockquote','code-block'],
//             ['formula','link'],
//             ['video','image'],
//             ['clean']
//         ]
//     },
//     theme:"snow"
// };

var options={
    modules: {
        syntax:true,
        formula: true,
        toolbar:{
            container: '#toolbar-container'
        }
    },
    theme:"snow"
};

function createQuill(){
    var quill = new Quill('#editor', options);
    $('[data-toggle="tooltip"]').tooltip();
    // quill.enableMathQuillFormulaAuthoring();
    quillShortcuts(quill);
    return quill
}
function quillShortcuts(quill){
    quill.keyboard.addBinding({
        key: 'C',
        shortKey: true,
        shiftKey: true
    }, function(range, context) {
        if(this.quill.getFormat(range)['code-block']){
            this.quill.format('code-block', false);
        }
        else {
            this.quill.format('code-block', true);
        }
    });
    quill.keyboard.addBinding({
        key: 'f',
        shortKey: true,
        shiftKey: true
    }, function(range, context) {
        this.quill.theme.tooltip.edit('formula');
    });
    quill.keyboard.addBinding({
        key: 'h',
        shortKey: true,
        shiftKey: true
    }, function(range, context) {
        if(this.quill.getFormat(range)['header']){
            this.quill.format('header', false);
        }
        else {
            this.quill.format('header', true);
        }
    });
}