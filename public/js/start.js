/**
 * Created by andrew on 8/4/17.
 */

$(document).ready(function(){
    buttonClick();
    buttonAnimation();
});
function buttonClick(){
    $(".type-panel").click(function(){
        var val = $($(this).find(".type-text")[0]).text().toLowerCase();
        sessionStorage.setItem('editor', val);
        window.location="editor.html";
    });
}
function buttonAnimation(){
    $(".type-panel").hover(function(){
        $(this).find(".type-icon").addClass("type-icon-hover");
        $(this).find(".type-text").addClass("type-text-hover");
    }, function(){
        $(this).find(".type-icon").removeClass("type-icon-hover");
        $(this).find(".type-text").removeClass("type-text-hover");
    });
}